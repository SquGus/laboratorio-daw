var pass1 = document.getElementById("password1"),
    pass2 = document.getElementById("password2"),
    butt = document.getElementById("send-button"),
    text = document.getElementById("test");

function check1() {
    if (pass1.value.length >= 8) {
        text.innerHTML = "Ahora confírma tu contraseña.<br>";
        butt.disabled = true;
    } else {
        text.innerHTML = "Por favor ingresa una contraseña con 8 caracteres mínimo.<br>";
        butt.disabled = true;
    }
}

function check2(){
    if (pass1.value == pass2.value) {
        text.innerHTML = "La contraseña coincide. Ya puedes enviar tu contraseña.<br>";
        butt.disabled = false;
    } else {
        text.innerHTML = "La contraseña no coincide.<br>";
        butt.disabled = true;
    }
}

function send(){
    text.innerHTML = "¡Enhorabuena! Tu contraseña ha sido registrada exitosamente.<br>";
}

pass1.onkeyup = check1;
pass2.onkeyup = check2;
butt.onclick = send;