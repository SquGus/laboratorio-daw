var droppingBalls = {
    max: 0,
    pelota: 0,
    profundidad: 0,
    solucion: 0,
    pos: function (padre, pelota, max) {
        var izq = padre * 2,
            der = izq + 1;
        if (izq < max && der < max) {
            if (pelota % 2 == 0) {
                pos(der, pelota / 2, max);
            } else {
                pos(izq, (pelota / 2) + 1, max);
            }
        } else {
            return padre;
        }
    }
}

droppingBalls.profundidad = Number(window.prompt("Cuál es la profundidad del árbol binario?"));
droppingBalls.pelota = Number(window.prompt("De qué pelota quieres conocer su hoja (cardinalidad)?"));
droppingBalls.max = Math.pow(2, droppingBalls.profundidad);
droppingBalls.solucion = droppingBalls.pos(1, droppingBalls.pelota, droppingBalls.max);

document.getElementById("solucion").innerHTML = "<br>Solución: " + droppingBalls.solucion;