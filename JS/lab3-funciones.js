var article = document.getElementsByTagName("article").item(0),
    text;
//PRIMERA FUNCIÓN
function primera() {
    "use strict";
    text = "Primera función<br>";
    var numero = window.prompt("Qué número quieres que sea calculado?"),
        i,
        potencias = [];
    for (i = 1; i <= numero; i += 1) {
        potencias.push(i);
        potencias.push(Math.pow(i, 2));
        potencias.push(Math.pow(i, 3));
    }
    for (i = 0; i < potencias.length; i += 1) {
        text += potencias[i] + " ";
    }
    text += "<br>";
    article.innerHTML = text;
}

//SEGUNDA FUNCIÓN
function segunda() {
    "use strict";
    text += "<br>Segunda función<br>";
    var a = Math.floor((Math.random() * 500) + 1),
        b = Math.floor((Math.random() * 500) + 1),
        c = a + b,
        tiempoInicial = new Date(),
        resultado = window.prompt("Cuánto resulta la suma de " + a + "+" + b + "?"),
        tiempoFinal = new Date(),
        diferencial = tiempoFinal.getUTCSeconds() - tiempoInicial.getUTCSeconds(),
        r,
        respuesta;
    if (resultado == c) {
        r = "correcto";
    } else {
        r = "incorrecto";
    }
    text += "El resultado fue " + r + ", mientras que tardaste " + diferencial + " segundos.<br>";
    article.innerHTML = text;
}

//TERCERA FUNCIÓN
function tercera() {
    "use strict";
    text += "<br>Tercera función<br>";
    var i,
        negativos = 0,
        ceros = 0,
        positivos = 0,
        arreglo = [],
        cantidad = window.prompt("Cuántos valores quieres en tu arreglo?");
    for (i = 0; i < cantidad; i += 1) {
        arreglo.push(window.prompt("Ingresa el valor #" + (i + 1)));
    }
    arreglo.sort(function (a, b) {return a - b});
    for (i = 0; i < cantidad; i += 1) {
        if (arreglo[i] < 0) {
            negativos += 1;
        } else {
            if (arreglo[i] == 0) {
                ceros += 1;
            } else {
                positivos = cantidad - (ceros + negativos);
            }
        }
    }
    text += "La cantidad de negativos es: " + negativos + "<br>La cantidad de ceros es: " + ceros + "<br>La cantidad de positivos es: " + positivos + "<br>";
    article.innerHTML = text;
}

//CUARTA FUNCIÓN
function cuarta() {
    "use strict";
    text += "<br>Cuarta función<br>";
    var i,
        j,
        total,
        matriz = [],
        promedios = [],
        cantidadArreglos = window.prompt("Cuántos arreglos quieres en tu matriz?"),
        cantidadNumeros;
    for (i = 0; i < cantidadArreglos; i += 1) {
        cantidadNumeros = window.prompt("Cuántos números quieres en tu arreglo #" + (i + 1) + "?");
        matriz[i] = new Array(cantidadNumeros);
        for (j = 0; j < cantidadNumeros; j += 1) {
            matriz[i][j] = window.prompt("Ingresa el valor #" + (j + 1) + " del arreglo #" + (i + 1));
        }
    }
    for (i = 0; i < cantidadArreglos; i += 1) {
        total = 0;
        for (j = 0; j < matriz[i].length; j += 1) {
            matriz[i][j] = Number(matriz[i][j]);
            total += matriz[i][j];
        }
        promedios[i] = (total / j);
        text += "El promedio del arreglo #" + (i + 1) + " es: " + promedios[i] + "<br>";
    }
    article.innerHTML = text;
    return promedios;
}

//QUINTA FUNCIÓN
function quinta() {
    "use strict";
    text += "<br>Quinta función<br>";
    var string = window.prompt("Qué número quieres invertir?"),
        arregloChar = [],
        i;
    for (i = 0; i < string.length; i += 1) {
        arregloChar[i] = string.charAt(i);
    }
    arregloChar.reverse();
    text += "";
    article.innerHTML = text;
    for (i = 0; i < string.length; i += 1) {
        text += arregloChar[i];
    }
    article.innerHTML = text;
}

primera();
segunda();
tercera();
cuarta();
quinta();