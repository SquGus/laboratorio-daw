var ejercicio = document.getElementById("ejercicio"),
    computadora = document.getElementById("computadora"),
    peso = document.getElementById("peso"),
    altura = document.getElementById("altura"),
    verduras = document.getElementById("verduras"),
    frutas = document.getElementById("frutas"),
    carbohidratos = document.getElementById("carbohidratos"),
    bebida = document.getElementById("bebida"),
    button = document.getElementById("submit"),
    IMC,
    salud = 0;

function check() {
    if (ejercicio.value != null &&
            computadora.value != null &&
            peso.value != null &&
            altura.value != null &&
            verduras.value != null &&
            frutas.value != null &&
            carbohidratos.value != null &&
            bebida.value != null) {
        button.disabled = false;
    }
}

function calcular() {
    window.alert("El botón ha sido presionado.");
    IMC = peso.value / Math.pow(altura, 2);
    //Calcula puntos por ejercicio
    if (ejercicio < 3) {
        salud += 1;
    } else if (ejercicio > 4) {
        salud += 3;
    } else {
        salud += 2;
    }
    //Calcula puntos por computadora
    if (computadora < 8) {
        salud += 3;
    } else if (computadora > 11) {
        salud += 1;
    } else {
        salud += 2;
    }
    //Calcula puntos por IMC
    if (IMC < 17 || IMC > 30) {
        salud += 1;
    } else if (IMC > 18.5 && IMC < 25) {
        salud += 3;
    } else {
        salud += 2;
    }
    //Calcula puntos por verduras
    salud += verduras.value;
    //Calcula puntos por frutas
    salud += frutas.value;
    //Calcula puntos por carbohidratos
    salud += carbohidratos.value;
    //Calcula puntos por bebida
    salud += bebida.value;
    
    //Calculo de puntos de salud
    salud = salud / 7;
    window.alert(salud);
}

ejercicio.onchange = check;
computadora.onchange = check;
peso.onchange = check;
altura.onchange = check;
verduras.onchange = check;
frutas.onchange = check;
carbohidratos.onchange = check;
bebida.onchange = check;
button.onclick = calcular;