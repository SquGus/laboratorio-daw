var name = document.getElementById("name"),
    opinion = document.getElementById("opinion"),
    help = document.getElementById("help"),
    button = document.getElementById("submit");

function showHintName() {
    "use strict";
    help.style.border = "2px solid #cccccc";
    help.style.top = "73px";
    help.style.boxShadow = "0px 2px 4px -2px #cccccc";
    help.style.backgroundColor = "white";
    help.style.padding = "5px";
    help.innerHTML = "Escribe tu nombre aquí, sólo es para referencia. No te secuestraremos, lo juro.";
}

function showHintOpinion() {
    "use strict";
    help.style.border = "2px solid #cccccc";
    help.style.boxShadow = "0px 2px 4px -2px #cccccc";
    help.style.backgroundColor = "white";
    help.style.padding = "5px";
    help.style.top = "130px";
    help.innerHTML = "Escribe la opinión que tengas sobre el sitio. ¡No temas, no lo usaremos en tu contra! Nos ayudarás a mejorar.";
}

function hideHint() {
    "use strict";
    help.style.top = "73px";
    help.innerHTML = "";
    help.style.border = "0px";
    help.style.boxShadow = "0px 0px 0px 0px #cccccc";
    help.style.padding = "0px";
}

function confirmation() {
    "use strict";
    help.style.top = "330px";
    help.style.color = "black";
    help.style.border = "2px solid red";
    help.style.boxShadow = "0px 2px 4px -2px #cccccc";
    help.style.backgroundColor = "orange";
    help.style.padding = "5px";
    help.innerHTML = "¿Estás seguro de lo que escribiste? Ya no hay vuelta atrás ¡eh!. No es cierto, pícale enviar :3";
}

function send() {
    "use strict";
    window.alert("¡Listo! Tu opinión ha sido enviada, gracias por aportar a la causa. ヘ(^0^)ヘ");
}

//NO SE PORQUÉ NO SIRVE EN "NAME"!!!!!!
//name.onmouseover = showHint;
//name.onmouseout = hideHint;

opinion.onmouseover = showHintOpinion;
//opinion.onfocus = showHintOpinion;
opinion.onmouseout = hideHint;
//opinion.onblur = hideHint;
button.onmouseover = confirmation;
button.onmouseout = hideHint;
button.onclick = send;