var dark = document.getElementById("dark"),
    hipster = document.getElementById("hipster"),
    alegre = document.getElementById("alegre"),
    chafa = document.getElementById("chafa"),
    changer = document.getElementById("changer");

function cambioDark() {
    changer.style.backgroundColor = "black";
    changer.style.color = "red";
    changer.style.fontFamily = "Trebuchet MS";
    changer.style.fontWeight = "bold";
}

function cambioHipster() {
    changer.style.backgroundColor = "darksalmon";
    changer.style.color = "brown";
    changer.style.fontFamily = "Comic Sans";
    changer.style.fontSize = ".8em";
    changer.style.textDecoration = "underline";
}

function cambioAlegre() {
    changer.style.backgroundColor = "orange";
    changer.style.color = "green";
    changer.style.fontWeight = "bold";
    changer.style.textDecoration = "none";
    changer.style.border = "2px dashed purple";
    changer.style.boxShadow = "box-shadow: 0px 4px 8px -4px black;";
}

function cambioChafa() {
    changer.style.backgroundColor = "aliceblue";
    changer.style.color = "black";
    changer.style.fontFamily = "Courier";
    changer.style.fontWeight = "normal";
    changer.style.textDecoration = "none";
    changer.style.fontSize = "3em";
    changer.style.border = "0px";
    changer.style.boxShadow = "none";
}

function regreso() {
    changer.style.backgroundColor = "floralwhite";
    changer.style.color = "black";
    changer.style.fontFamily = "Helvetica, Arial, sans-serif";
    changer.style.fontWeight = "normal";
    changer.style.textDecoration = "none";
    changer.style.fontSize = "100%";
    changer.style.border = "2px solid #cccccc";
    changer.style.boxShadow = "box-shadow: 0px 2px 4px -2px #cccccc;";
}

dark.onmouseover = cambioDark;
dark.onmouseout = regreso;
hipster.onmouseover = cambioHipster;
hipster.onmouseout = regreso;
alegre.onmouseover = cambioAlegre;
alegre.onmouseout = regreso;
chafa.onmouseover = cambioChafa;
chafa.onmouseout = regreso;