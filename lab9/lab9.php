<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #9</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab9-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #9 <small>Introducción</small></h3>
                <p>Este es el laboratorio #9 de DAW. Aquí aprenderé lo más importante sobre el manejador de sesiones que tiene PHP, y cómo es que se puede enviar o recibir información a través de sesiones establecidas en alguna página de un sitio web. Practicaré este tema al crear una forma donde sea necesario subir una imagen para completar el registro, haciendo uso de sesiones y el método Post. Finalmente responderé algunas preguntas que refuerzen mi aprendizaje.</p>
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab9-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>