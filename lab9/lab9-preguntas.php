<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #9</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab9-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #9 <small>Preguntas</small></h3>
                
                <!-- MAIN CONTENT -->
                <ul class="no-bullet">
                    <li><strong>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</strong>
                        <p>Para remover todas las variables globales contenidas en $_SESSION y destruir la sesión y con ella, información sensible contenida en ésta. Más que nada es como medida de seguridad.</p>
                    </li>
                    <li><strong>¿Cuál es la diferencia entre una variable de sesión y una cookie?</strong>
                        <p>Ambas cumplen el propósito de almacenar información importante para tu sistema entre páginas del sitio web. La diferencia más importante es que las cookies se guardan en el lado del cliente, pudiendo durar meses o hasta años en el ordenador del usuario. Por el otro lado, las sessiones se almacenan del lado del servidor, esta información no puede ser modificada por el usuario y no hay un límite del tamaño de la información almancenada en la sesión de un usuario.</p>
                    </li>
                    <li><strong>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</strong>
                        <p>Al momento de guardar FB una imagen en su gran base de datos, se encarga de guardar la imagen con un nombre/dirección/número que sea único; este proceso se controla a partir de una especie de contador que evita que dos fotos compartan la misma dirección. Entonces, si se llegasen a subir dos imágenes con el mismo nombre, FB las habrá guardado con distintos nombres, tal vez solamente sean guardadas con números.</p>
                    </li>
                </ul>
                <strong>Referencias:</strong>
                    <ul>
                        <li>http://www.hackingwithphp.com/10/1/0/cookies-vs-sessions</li>
                    </ul>
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab9-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>