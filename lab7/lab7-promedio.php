<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #7</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab7-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #7 <small>Promedio/Mediana</small></h3>
                <?php
function estadisticas($numbers, $quantity){
    
    //Arreglo original
    echo "<ul><li>Arreglo de números:<ul>";
    for($i=0; $i<$quantity; $i++){
        echo "<li>".$numbers[$i]."</li>";
    }
    echo "</ul></li>";
    
    //Menor a mayor
    sort ($numbers);
    echo "<li>Arreglo de números menor a mayor:<ul>";
    for($i=0; $i<$quantity; $i++){
        echo "<li>".$numbers[$i]."</li>";
    }
    echo "</ul></li>";
    
    //Mayor a menor
    rsort ($numbers);
    echo "<li>Arreglo de números mayor a menor:<ul>";
    for($i=0; $i<$quantity; $i++){
        echo "<li>".$numbers[$i]."</li>";
    }
    echo "</ul></li>";
    
    //Promedio
    $average = 0;
    for($i=0; $i<$quantity; $i++){
        $average += $numbers[$i];
    }
    $average = $average/$quantity;
    echo "<li>Promedio: ".$average."</li>";
    
    //Mediana
    $median = $quantity/2;
    echo "<li>Mediana: ";
    if($quantity%2 == 0){
        echo ($numbers[$median-1]+$numbers[$median])/2;
    }else{
        echo $number[$median];
    }
    echo "</li></ul>";
}


$numbers = array(
    array(100, 95, 88, 97, 96, 99),
    array(95, 78, 98, 88, 87, 100)
    );

for($n=0; $n<count($numbers); $n++){
    echo "Set #".($n+1);
    estadisticas($numbers[$n], count($numbers[$n]));
}

                ?>
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab7-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>