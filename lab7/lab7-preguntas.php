<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #7</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab7-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #7 <small>Preguntas</small></h3>
                
                <!-- MAIN CONTENT -->
                <ul class="no-bullet">
                    <li><strong>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</strong>
                        <p>Muestra gran cantidad de información sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP, información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de configuración locales y generales, cabeceras HTTP y licencia de PHP.<br>Me llamó la atención mucho la información que despliega sobre el servidor en el cual está corriendo PHP y todas las demás extensiones, también el hecho de que tenga una extensión de json y MySQL; finalmente me impresionó la gran cantidad de extensiones con las que cuenta.</p>
                    </li>
                    <li><strong>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?
</strong>
                        <p>Aunque no es recomendado usar XAMPP como un servidor Apache con PHP incluído en un entorno de producción, es posible hacerlo. Es necesario hacer cambios en la configuración tanto de Apache como en el archivo config.php.</p>
                    </li>
                    <li><strong>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.
</strong>
                        <p>Esto se debe a las propiedades de PHP y en sí, al objetivo por el cual fue hecho este lenguaje de programación. Pues así como está escrito en el sitio oficial de PHP, este es un lenguaje de scripting que puede ser embebido en HTML (contenido), su objetivo es permitir a los desarrolladores web escribir con rapidez páginas generadas dinámicamente y está alojado en el lado del servidor.</p>
                    </li>
                </ul>
                <strong>Referencias:</strong>
                    <ul>
                        <li>http://php.net/manual/es/faq.general.php</li>
                        <li>http://php.net/manual/es/install.general.php</li>
                    </ul>
                
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab7-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>