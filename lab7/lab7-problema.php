<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #7</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab7-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #7 <small>Problema</small></h3>
                
                <!-- MAIN CONTENT -->
                <strong>Problema 12405 de UVa - Scarecrow</strong>
                <p>
                    <h5>Description</h5>
                Taso owns a very long field. He plans to grow different types of crops in the upcoming growing season. The area, however, is full of crows and Taso fears that they might feed on most of the crops.
For this reason, he has decided to place some scarecrows at different locations of the field. The field can be modeled as a 1×N grid. Some parts of the field are infertile and that means you cannot grow any crops on them. A scarecrow, when placed on a spot, covers the cell to its immediate left and right along with the cell it is on. Given the description of the field, what is the minimum number of scarecrows that needs to be placed so that all the useful section of the field is covered? Useful section refers to cells where crops can be grown.
                <h5>Input</h5>
                Input starts with an integer T (≤ 100), denoting the number of test cases. Each case starts with a line containing an integer N (0 < N < 100). The next line contains N characters that describe the field. A dot (.) indicates a crop-growing spot and a hash (#) indicates an infertile region.
                <h5>Output</h5>
                For each case, output the case number first followed by the number of scarecrows that need to be placed.</p>
                <?php
$cases = 3;
$crops = array(
    array(".",".","#",".","#",".","#","#",".",".",".","#","#",".",),
    array("#",".","#",".",".","#",".",".","#","#",".",".","#","#",),
    array("#","#","#","#","#","#",".",".",".",".",".",".",".",".",));

for($i=0; $i<$cases; $i++){
    echo "Case #".($i+1)."<br>";
    for($j=0; $j<count($crops[$i]); $j++){
        echo $crops[$i][$j];
    }
    $total = 0;
    echo "<strong>";
    for($j=0; $j<count($crops[$i]); $j++){
        echo $crops[$i][$j];
        if($crops[$i][$j] == "."){
            $j += 2;
            $total++;
        }
    }
    echo "</strong>";
    echo "<br>Total of scarecrows: ".$total;
    if($i+1 != $cases){
        echo "<br><br>";
    }
}
                ?>
                
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab7-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>