<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Home</title>
        <link rel="stylesheet" href="CSS/foundation.css">
        <link rel="stylesheet" href="CSS/app.css">
        <script src="JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            <div class="large-9 columns">
            </div>
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Desarrollo de Aplicaciones Web <small>Home</small></h3>
                <p>En este sitio web se encuentran todos los laboratorios que iré haciendo de la materia Desarrollo de Aplicaciones Web. Este sitio está en constante cambio y construcción, probando mi aprendizaje de este curso.</p>
            </div>
        
            <div class="large-3 pull-9 columns">
                <ul class="side-nav">
                    <li><a href="/lab1/lab1.php">Lab 1</a></li>
                    <li><a href="/lab2/lab2.php">Lab 2</a></li>
                    <li><a href="/lab3/lab3.php">Lab 3</a></li>
                    <li><a href="/lab4/lab4.php">Lab 4</a></li>
                    <li><a href="/lab5/lab5.php">Lab 5</a></li>
                    <li><a href="/lab6/lab6.php">Lab 6</a></li>
                    <li><a href="/lab7/lab7.php">Lab 7</a></li>
                    <li><a href="/lab8/lab8.php">Lab 8</a></li>
                    <li><a href="/lab9/lab9.php">Lab 9</a></li>
                </ul>
                <p><img src="IMG/logo.png"/></p>
</div>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    <div class="large-6 columns">
                    </div>
                </div>
            </div> 
        </footer>
        <script src="JS/vendor/jquery.js"></script>
        <script src="JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>