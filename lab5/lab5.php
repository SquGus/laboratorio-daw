<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #5</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab5-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #5 <small>Introducción</small></h3>
                <p>Este es el laboratorio #5 de DAW, como el laboratorio pasado, profundizaré en el conocimiento de HTML, CSS y JavaScript, através de pequeñas pero curiosas aplicaciones que están disponibles en los links de aquí abajo. Por último, responderé unas cuantas preguntas como es de costumbre, para reafirmar mi aprendizaje.</p>
            </div>
        
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab5-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="/JS/vendor/jquery.js"></script>
        <script src="/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>