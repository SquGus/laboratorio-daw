<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #8</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab8-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #8 <small>Introducción</small></h3>
                <p>Este es el laboratorio #8 de DAW. Al realizar este laboratorio continuaré aprendiendo del lenguaje de programación PHP, así mismo tendré que aplicar el modelo de capas, el cual se divide en Modelo-Vista-Controlador y sirve para mantener los procesos separados de lo que al usuario se le presenta en su ordenador. Para concluir, unas cuantas preguntas serán respondidas para confirmar que sí cumplí el objetivo de este laboratorio.</p>
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab8-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>