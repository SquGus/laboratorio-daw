<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #8</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab8-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #8 <small>Inscripción a INE</small></h3>
                
                <!-- MAIN CONTENT -->
                Si estás interesado en formar parte del INE, necesitamos recabar un poco de información sobre ti. Nosotros te contactaremos por teléfono y haremos una cita contigo para seguir con el proceso de reclutamiento. <strong>Llena todos los datos para que puedas enviarnos la información.</strong><br><br>
                <form method=
"post" action="lab8-controlador.php">
                    Nombre completo:
                    <input type="text" name="nombre" placeholder="Nombre" autocomplete="off">
                    <input type="text" name="apellidop" placeholder="Apellido paterno" autocomplete="off">
                    <input type="text" name="apellidom" placeholder="Apellido materno" autocomplete="off">
                    <div class="row">
                        <div class="large-3 columns">
                            Sexo:<br>
                            <input type="radio" name="sexo" value="hombre"> Hombre<br>
                            <input type="radio" name="sexo" value="mujer"> Mujer<br>
                        </div>
                        
                        <div class="large-4 columns">
                            Correo:<br>
                            <input type="email" name="correo" placeholder="tucorreo@mail.com" autocomplete="off">
                            Teléfono celular:
                            <input type="tel" name="telefono" placeholder="(123) 456 7890" minlength="10" maxlength="10" autocomplete="off">
                        </div>
                        <div class="large-5 columns">
                            ¿A qué partido sigues?
                            <select name="partido">
                                <option value="none">Partido</option>
                                <option value="PAN">PAN</option>
                                <option value="PRD">PRD</option>
                                <option value="PRI">PRI</option>
                                <option value="PVEM">PVEM</option>
                                <option value="PANAL">PANAL</option>
                            </select>
                            <input type="text" name="otroPartido" placeholder="Escribe aquí las siglas si es otro partido" autocomplete="off">
                        </div>
                    </div>
                    
                    ¿Qué día puedes asistir a nuestras oficinas?
                    <input type="date" name="fecha">
                    <button type="submit" name="submit" value="Enviar">Enviar</button>
                    <button type="reset" name="reset" value="Borrar">Borrar</button>
                </form>
            
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab8-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>