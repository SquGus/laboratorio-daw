<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #8</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab8-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #8 <small>Preguntas</small></h3>
                
                <!-- MAIN CONTENT -->
                <ul class="no-bullet">
                    <li><strong>¿Por qué es una buena práctica separar el controlador de la vista?.</strong>
                        <p>Porque divide la forma en que se procesa la información de la manera en que se presenta al usuario dicha información. Este modelo fue propuesto para aplicaciones desktop, pero ha sido muy popular su implementación en aplicaciones web.</p>
                    </li>
                    <li><strong>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</strong>
                        <ul>
                            <li>Superglobals — Superglobals son variables internas que están disponibles siempre en todos los ámbitos</li>
                            <li>$GLOBALS — Hace referencia a todas las variables disponibles en el ámbito global</li>
                            <li>$_SERVER — Información del entorno del servidor y de ejecución</li>
                            <li>$_GET — Variables HTTP GET</li>
                            <li>$_POST — Variables POST de HTTP</li>
                            <li>$_FILES — Variables de Carga de Archivos HTTP</li>
                            <li>$_REQUEST — Variables HTTP Request</li>
                            <li>$_SESSION — Variables de sesión</li>
                            <li>$_ENV — Variables de entorno</li>
                            <li>$_COOKIE — Cookies HTTP</li>
                            <li>$php_errormsg — El mensaje de error anterior</li>
                            <li>$HTTP_RAW_POST_DATA — Datos POST sin tratar</li>
                            <li>$http_response_header — Encabezados de respuesta HTTP</li>
                            <li>$argc — El número de argumentos pasados a un script</li>
                            <li>$argv — Array de argumentos pasados a un script</li>
                        </ul><br>
                    </li>
                    <li><strong>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.
</strong>
                        <p>Más que funciones, me llamaron la atención 2 API's que al parecer son muy poderosas. El primer API es "MCVE", el cual se encarga de procesar la información de tarjetas de crédito/débito/regalo en PHP. El otro API interesante es "Password Hashing", con el cual es posible manipular contraseñas de manera más segura dentro del sistema.</p>
                    </li>
                </ul>
                <strong>Referencias:</strong>
                    <ul>
                        <li>https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller</li>
                        <li>http://php.net/manual/en/intro.mcve.php</li>
                        <li>http://php.net/manual/en/intro.password.php</li>
                    </ul>
                
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab8-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>