<?php

//Definición de variables
$nombreE = $apellidopE = $apellidomE = $sexoE = $correoE = $telefonoE = $partidoE = $fechaE = "";
$nombre = $apellidop = $apellidom = $sexo = $correo = $telefono = $partido = $fecha = "";

function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    //Chequeo nombre completo
    if(empty($_POST["nombre"]) || empty($_POST["apellidop"]) || empty($_POST["apellidom"])){
        $nombreE = "Se requiere el nombre completo.";
    }else{
        //Chequeo a nombre
        $nombre = test_input($_POST["nombre"]);
        if(!preg_match("/^[a-zA-Z ]*$/", $nombre)){
            $nombreE = "Sólo se permiten letras y espacios.";
        }
        
        //Chequeo a apellido paterno
        $apellidop = test_input($_POST["apellidop"]);
        if(!preg_match("/^[a-zA-Z ]*$/", $apellidop)){
            $apellidopE = "Sólo se permiten letras y espacios.";
        }
                
        //Chequeo a apellido materno
        $apellidom = test_input($_POST["apellidom"]);
        if(!preg_match("/^[a-zA-Z ]*$/", $apellidom)) {
            $apellidomE = "Sólo se permiten letras y espacios.";
        }
    }
    
    //Chequeo sexo
    if (empty($_POST["sexo"])){
        $sexoE = "Requerido.";
    }else{
        $sexo = test_input($_POST["sexo"]);
    }
    
    //Chequeo email
    if(empty($_POST["correo"])){
        $correoE = "Requerido.";
    }else{
        $correo = test_input($_POST["correo"]);
        if(!filter_var($correo, FILTER_VALIDATE_EMAIL)){
            $correoE = "El formato del correo es inválido.";
        }
    }
    
    //Chequeo teléfono
    if(empty($_POST["telefono"])){
        $telefonoE = "Requerido.";
    }else{
        $telefono = test_input($_POST["telefono"]);
        if(!preg_match("/^[0-9]*$/", $telefono)){
            $telefonoE = "Sólo números.";
        }
    }
    
    //Chequeo partido
    if($_POST["partido"]=="none" && empty($_POST["otroPartido"])){
        $partidoE = "Requerido.";
    }else{
        if($_POST["partido"]!="none"){
            $partido = test_input($_POST["partido"]);
        }else if(!empty($_POST["otroPartido"])){
            $partido = test_input($_POST["otroPartido"]);            
            if(!preg_match("/^[A-Z]*$/", $partido)){
                $partidoE = "Sólo se permiten siglas en letras mayúsculas.";
            }
        }
    }
    
    //Chequeo fecha
    if(empty($_POST["fecha"])){
        $fechaE = "Fecha requerida.";
    }else{
        $fecha = test_input($_POST["fecha"]);
    }
}

//DESPLIEGUE DE INFORMACIÓN
if($nombreE == "" 
   && $apellidopE == "" 
   && $apellidomE == "" 
   && $sexoE == "" 
   && $correoE == "" 
   && $telefonoE == "" 
   && $partidoE == "" 
   && $fechaE == ""){
    
    echo '<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #8</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include "lab8-links.php"; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #8 <small>Inscripción a INE</small></h3>
                
                <!-- MAIN CONTENT -->
                <strong>¡Éxito! </strong>Haz logrado enviar tu solicitud, ahora espera nuestra llamada para poder agendar una cita.<br><br>
                Tus datos son los siguientes:<br>
                <ul>
                    <li>Nombre completo: '.$nombre.' '.$apellidop.' '.$apellidom.'</li>
                    <li>Sexo: '.$sexo.'</li>
                    <li>Correo: '.$correo.'</li>
                    <li>Teléfono celular: '.$telefono.'</li>
                    <li>Partido político: '.$partido.'</li>
                    <li>Fecha disponible: '.$fecha.'</li>
                </ul>
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include "../__nav__.php"; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include "lab8-links.php"; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>';
    
}
else{
    echo '<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #8</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include "lab8-links.php"; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #8 <small>Inscripción a INE</small></h3>
                
                <!-- MAIN CONTENT -->
                Si estás interesado en formar parte del INE, necesitamos recabar un poco de información sobre ti. Nosotros te contactaremos por teléfono y haremos una cita contigo para seguir con el proceso de reclutamiento. <strong>Llena todos los datos para que puedas enviarnos la información.</strong><br><br>
                <form method=
"post" action="lab8-controlador.php">
                    Nombre completo: <small style="color: red">'.$nombreE.'</small>
                    <input type="text" name="nombre" placeholder="Nombre" autocomplete="off" value="'.test_input($_POST["nombre"]).'">
                    <small style="color: red">'.$apellidopE.'</small>
                    <input type="text" name="apellidop" placeholder="Apellido paterno" autocomplete="off" value="'.test_input($_POST["apellidop"]).'">
                    <small style="color: red">'.$apellidomE.'</small>
                    <input type="text" name="apellidom" placeholder="Apellido materno" autocomplete="off" value="'.test_input($_POST["apellidom"]).'">
                    <div class="row">
                        <div class="large-3 columns">
                            Sexo: <small style="color: red">'.$sexoE.'</small><br>
                            <input type="radio" name="sexo" value="hombre"';if($sexo == "hombre"){
        echo 'checked';}echo '> Hombre<br>
                            <input type="radio" name="sexo" value="mujer"';if($sexo == "mujer"){
        echo 'checked';}echo '> Mujer<br>
                        </div>
                        
                        <div class="large-4 columns">
                            Correo: <small style="color: red">'.$correoE.'</small><br>
                            <input type="email" name="correo" placeholder="tucorreo@mail.com" autocomplete="off" value="'.test_input($_POST["correo"]).'">
                            Teléfono celular: <small style="color: red">'.$telefonoE.'</small>
                            <input type="tel" name="telefono" placeholder="(123) 456 7890" minlength="10" maxlength="10" autocomplete="off" value="'.test_input($_POST["telefono"]).'">
                        </div>
                        <div class="large-5 columns">
                            ¿A qué partido sigues? <small style="color: red">'.$partidoE.'</small>
                            <select name="partido">
                                <option value="none"';if(test_input($_POST["partido"])=="none"){echo ' selected';}echo'>Partido</option>
                                <option value="PAN"';if(test_input($_POST["partido"])=="PAN"){echo ' selected';}echo'>PAN</option>
                                <option value="PRD"';if(test_input($_POST["partido"])=="PRD"){echo ' selected';}echo'>PRD</option>
                                <option value="PRI"';if(test_input($_POST["partido"])=="PRI"){echo ' selected';}echo'>PRI</option>
                                <option value="PVEM"';if(test_input($_POST["partido"])=="PVEM"){echo ' selected';}echo'>PVEM</option>
                                <option value="PANAL"';if(test_input($_POST["partido"])=="PANAL"){echo ' selected';}echo'>PANAL</option>
                            </select>
                            <input type="text" name="otroPartido" placeholder="Escribe aquí las siglas si es otro partido" autocomplete="off" value="'.test_input($_POST["otroPartido"]).'">
                        </div>
                    </div>
                    
                    ¿Qué día puedes asistir a nuestras oficinas? <small style="color: red">'.$fechaE.'</small>
                    <input type="date" name="fecha" value="">
                    <button type="submit" name="submit" value="Enviar">Enviar</button>
                    <button type="reset" name="reset" value="Borrar">Borrar</button>
                </form>
            
            </div>
            
            <!-- NAVIGATION BAR -->
            <?php include "../__nav__.php"; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include "lab8-links.php"; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>
    ';
}
?>