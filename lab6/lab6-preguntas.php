<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab6</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="css/app.css">
        <script src="/JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            <?php include 'lab6-links.php'; ?>
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #6 <small>Material Design</small></h3>
                <ul class="no-bullet">
                    <li><strong>¿Qué es Material Design? </strong>
                        <p>Material Design es una guía hecha por Google que abarca aspectos visuales, de movimiento y de interacción que se recomiendan sean implementados en aplicaciones de Android, Android Wear, TV, automóviles y cualquier dispositivo relacionado con Google en general.</p>
                        <p>Esta nueva especificación introduce varios nuevos elementos a la experiencia de usuario como:</p>
                        <ul class="square">
                            <li>* Nuevo tema</li>
                            <li>* Nuevos widgets</li>
                            <li>* Nuevo API para animaciones y sombras</li>
                            <li>* Guía de colores recomendados</li>
                            <li>* Entre muchas otras cosas que general una experiencia uniforme y más consistente</li>
                        </ul>
                        <p>Así pues, esta guía ha sido adoptada gradualmente por los desarrolladores de Android y demás aplicaciones para dispositivos de Google. Material llegó para quedarse, al ser anunciado en Google I/O el 25 de junio de 2014 y ya ha sido adoptado casi en la totalidad de las principales aplicaciones de la gigante del Internet.</p>
                    </li>
                </ul>
                <strong>Referencias:</strong>
                    <ul>
                        <li>https://www.google.com/design/spec/material-design/introduction.html#</li>
                        <li>https://es.wikipedia.org/wiki/Material_design</li>
                        <li>https://developer.android.com/design/material/index.html</li>
                </ul>
            </div>
        
            <?php include '../__nav__.php' ?>
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    <div class="large-6 columns">
                        <ul class="inline-list right">
                            <li><a href="../lab6/lab6-preguntas.html">Material Design</a></li>
                        </ul>
                    </div>
                </div>
            </div> 
        </footer>
        <script src="/JS/vendor/jquery.js"></script>
        <script src="/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>