<!DOCTYPE html>
<!-- Laboratorios de Desarrollo de Aplicaciones Web -->
<!-- Hecho por Gustavo Gutiérrez Gómez -->

<html>
    <head>
        <meta charset="UTF-8">
        <title>DAW - Lab #6</title>
        <link rel="stylesheet" href="../CSS/foundation.css">
        <link rel="stylesheet" href="../CSS/app.css">
        <script src="../JS/vendor/modernizr.js"></script>
    </head>
    <body>
        
        <div class="row">
            <div class="large-3 columns">
                <h1><a href="../index.php">ヽ( ˘∪˘ )ゝ</a></h1>
            </div>
            
            <!-- LINKS BAR -->
            <?php include 'lab6-links.php'; ?>
            
        </div>
        
        <div class="row">    
            <div class="large-9 push-3 columns">
                <h3>Lab #6 <small>Introducción</small></h3>
                <p>Este es el laboratorio #6 de DAW, en el cual aplicaré un Framework proveniente de "Foundation" (algo así como una plantilla HTML con estilos incluidos). Aplicaré este Framework a todo el sitio, y terminaré dando un pequeño resumen sobre lo que representa Material Design propuesto por Google.</p>
            </div>
        
            <!-- NAVIGATION BAR -->
            <?php include '../__nav__.php'; ?>
            
        </div>
        
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p>© SquGus 2015</p>
                    </div>
                    
                    <!-- LINKS BAR -->
                    <?php include 'lab6-links.php'; ?>
                    
                </div>
            </div> 
        </footer>
        <script src="../JS/vendor/jquery.js"></script>
        <script src="../JS/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>